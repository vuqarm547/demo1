# -*- mode: ruby -*-
# vi: set ft=ruby :

#vagrant-hostmanager plugin: Manages the /etc/hosts file on guest machines
#vagrant-reload plugin: Is usually required for changes made in Vagrantfile to take effect
#vagrant-cachier: Reduce network usage and speed up local Vagrant-based provisioning, drastically improving VM provisioning times.
#This is vagrant plugin installation
if ARGV[0] != 'plugin'
  
    required_plugins = ['vagrant-hostmanager', 'vagrant-reload',
                        'vagrant-cachier', 'vagrant-faster'
    ]
    plugins_to_install = required_plugins.select { |plugin| not Vagrant.has_plugin? plugin }
    if not plugins_to_install.empty?
      puts "Installing plugins: #{plugins_to_install.join(' ')}"
        if system "vagrant plugin install #{plugins_to_install.join(' ')}"
          exec "vagrant #{ARGV.join(' ')}"
        else
          abort "Installation of one or more plugins has failed. Aborting."
        end
  
    end
end

#Generating public and private key pairs and using this key pairs on my machine
$global = <<-SCRIPT
#check for private key for vm-vm comm
[ -f /vagrant/id_rsa ] || {
  ssh-keygen -t rsa -f /vagrant/id_rsa -q -N ''
}
#deploy key
[ -f /home/vagrant/.ssh/id_rsa ] || {
    cp /vagrant/id_rsa /home/vagrant/.ssh/id_rsa
    chmod 0600 /home/vagrant/.ssh/id_rsa
}
#allow ssh passwordless
grep 'vagrant@node' ~/.ssh/authorized_keys &>/dev/null || {
  cat /vagrant/id_rsa.pub >> ~/.ssh/authorized_keys
  chmod 0600 ~/.ssh/authorized_keys
}
#exclude node* from host checking
cat > ~/.ssh/config <<EOF
Host node*
   StrictHostKeyChecking no
   UserKnownHostsFile=/dev/null
EOF

#end script

# Create APP user
SCRIPT
$adduser = <<-SCRIPT

useradd -m -s /bin/bash -U app_user -u 666 --groups wheel
cp -pr /home/vagrant/.ssh /home/app_user/
chown -R app_user:app_user /home/app_user
echo "%app_user ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/app_user

SCRIPT
$initScript = <<-SCRIPT
  export SCRIPT_HOME=/vagrant
  echo _______________________________________________________________________________
  echo 0. Prepare env mysql
  source $SCRIPT_HOME/env.sh
  echo _______________________________________________________________________________
  echo 1. Install and setup java
  /bin/bash $SCRIPT_HOME/java.sh

SCRIPT

$systemd = <<-SCRIPT
cat > /etc/systemd/system/petclinic.service <<EOF
[Unit]
Description=Petclinic Java Spring Boot
[Service]
User=vagrant
Environment="MYSQL_PASS=1edc2wsx3qaz@"
Environment="MYSQL_URL=jdbc:mysql://db:3306/demo1"
Environment="MYSQL_USER=demo1"

  # The configuration file application.properties should be here:#change this to your workspace
WorkingDirectory=/home/vagrant/demo1

  #path to executable.
  #executable is a bash script which calls jar file
ExecStart=/bin/java -Xms128m -Xmx256m -Dspring.profiles.active=mysql -jar /home/vagrant/demo1/target/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar

SuccessExitStatus=143
TimeoutStopSec=10
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target

EOF

  sudo systemctl daemon-reload
  sudo systemctl enable petclinic.service
  sudo systemctl start petclinic

SCRIPT

Vagrant.configure("2") do |config|

  config.vm.provision "shell", privileged: false, inline: $global
  config.vm.synced_folder ".", "/vagrant", type: "nfs"
  config.vm.boot_timeout = 1800

    if Vagrant.has_plugin?("vagrant-hostmanager")
        config.hostmanager.enabled = true
        config.hostmanager.manage_host = true
        config.hostmanager.ignore_private_ip = false
        config.hostmanager.include_offline = true
    end

    config.vm.define :DB_VM do |db| 
        db.vm.box = "ubuntu/focal64"
        db.vm.hostname = "db"
        db.vm.provision :shell, path: "db.sh"
        db.vm.provision :shell, path: "check-mysql.sh"        
        db.vm.network :private_network, ip: "192.168.33.10" 
        db.vm.network "forwarded_port", guest: 3306, host: 8002,
        auto_correct: true
        db.vm.usable_port_range = 8000..8999
        db.vm.provider "virtualbox" do |vb|
            vb.name = "DB_VM"
            vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
            vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
            vb.customize ["modifyvm", :id, "--ioapic", "on"]
            vb.memory = "3072"
            vb.cpus = "3"
        end
    end 

    config.vm.define :APP_VM do |app| 
        app.vm.box = "ubuntu/focal64" 
        app.vm.hostname = "app"
        app.vm.provision "shell", inline: $adduser
        app.vm.provision "shell", inline: $initScript
        app.vm.provision "shell", inline: $systemd
        app.vm.network :private_network, ip: "192.168.33.11" 
        app.vm.network "forwarded_port", guest: 8080, host: 8001,
        auto_correct: true 
        app.vm.usable_port_range = 8000..8999
        app.vm.provider "virtualbox" do |vb|
            vb.name = "APP_VM"
            vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
            vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
            vb.customize ["modifyvm", :id, "--ioapic", "on"]
            vb.memory = "3072"
            vb.cpus = "3"
        end
    end
end
